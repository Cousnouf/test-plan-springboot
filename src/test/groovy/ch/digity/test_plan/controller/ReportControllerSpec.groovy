package ch.digity.test_plan.controller


import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
class ReportControllerSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    def "should call properly the controller and get the result"() {
        given:
        File inputFile = new File("src/test/resources/export_zephyr/ZFJ-Executions-09-15-2021_3.csv")
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "ZFJ-Executions-09-15-2021_3.csv", MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(inputFile))

        when:
        def resultActionsCreation = mockMvc.perform(
                multipart("/report/generate?dateFrom=2021-06-07&dateTo=2021-06-30&numberFormat=ABSOLUTE")
                        .file(mockMultipartFile)
        )

        then:
        resultActionsCreation.andExpect(status().isCreated())

        and:
        def location = resultActionsCreation.andReturn().getResponse().getHeader("Location")
        def resultActionsGet = mockMvc.perform(get(location))

        then:
        resultActionsGet.andExpect(status().isOk())
        def inputStream = new ByteArrayInputStream(resultActionsGet.andReturn().getResponse().getContentAsByteArray())
        def workbook = new XSSFWorkbook(inputStream)

        then: 'we check the first tab, by cycle name'
        def byCycleName = workbook.getSheetAt(1)
        def cpt = 0
        checkLine(byCycleName, cpt++, "Cycle name", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total")
        checkLine(byCycleName, cpt++, "CCFE_Exchanges", 0, 10, 1, 0, 10, 21)
        checkLine(byCycleName, cpt++, "CCFE_Order Creation", 42, 18, 1, 1, 0, 62)
        checkLine(byCycleName, cpt++, "CCFE_Order creation", 1, 0, 0, 0, 0, 1)
        checkLine(byCycleName, cpt++, "CCFE_Replacement", 0, 4, 0, 0, 4, 8)
        checkLine(byCycleName, cpt++, "CCFE_Returns", 0, 2, 0, 0, 17, 19)
        checkLine(byCycleName, cpt++, "CCFE_SWSE Toggle Feature", 6, 0, 0, 0, 0, 6)
        checkLine(byCycleName, cpt++, "Emails", 17, 2, 5, 1, 9, 34)
        checkLine(byCycleName, cpt++, "Extend_Order Creation", 1, 3, 0, 0, 1, 5)
        checkLine(byCycleName, cpt++, "Extend_Retail", 3, 0, 1, 0, 0, 4)
        checkLine(byCycleName, cpt++, "Extend_WHS", 1, 0, 0, 0, 0, 1)
        checkLine(byCycleName, cpt++, "HIND_CCFE_Order Creation", 18, 7, 1, 3, 2, 31)
        checkLine(byCycleName, cpt++, "HIND_CCFE_SWSE Toggle Feature", 1, 0, 0, 0, 0, 1)
        checkLine(byCycleName, cpt++, "Website_Order Creation", 1, 3, 0, 10, 11, 25)
        checkLine(byCycleName, cpt++, "Website_Order creation", 1, 0, 0, 1, 2, 4)
        checkLine(byCycleName, cpt++, "Website_SWSE Toggle Feature", 0, 0, 0, 2, 2, 4)
        checkLine(byCycleName, cpt++, "YNAP_Order Creation", 5, 4, 0, 0, 1, 10)
        checkLine(byCycleName, cpt, "YNAP_SWSE Toggle Feature", 0, 0, 0, 0, 1, 1)

        then: 'we check the fourth tab, per maison and country'
        def perMaisonAndCountry = workbook.getSheetAt(3)
        def cpt2 = 0
        checkLine(perMaisonAndCountry, cpt2++, "Maison - Country", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total")
        checkLine(perMaisonAndCountry, cpt2++, "ALS - IT", 8, 3, 0, 0, 0, 11)
        checkLine(perMaisonAndCountry, cpt2++, "ALS - SE", 2, 1, 0, 0, 2, 5)
        checkLine(perMaisonAndCountry, cpt2++, "BEM - AT", 3, 1, 1, 0, 1, 6)
        checkLine(perMaisonAndCountry, cpt2++, "BEM - IT", 9, 3, 0, 0, 0, 12)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - AT", 0, 1, 0, 0, 1, 2)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - BE", 1, 1, 1, 0, 0, 3)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - CZ", 1, 2, 1, 0, 3, 7)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - DK", 1, 0, 0, 0, 0, 1)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - IT", 15, 5, 2, 0, 8, 30)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - LU", 1, 0, 0, 0, 0, 1)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - NL", 0, 2, 0, 0, 0, 2)
        checkLine(perMaisonAndCountry, cpt2++, "CAR - SE", 0, 1, 0, 0, 0, 1)
        checkLine(perMaisonAndCountry, cpt2++, "IWC - BE", 2, 2, 1, 0, 1, 6)
        checkLine(perMaisonAndCountry, cpt2++, "IWC - IT", 11, 3, 0, 1, 3, 18)
        checkLine(perMaisonAndCountry, cpt2++, "JLC - DK", 2, 1, 3, 1, 1, 8)
        checkLine(perMaisonAndCountry, cpt2++, "JLC - IT", 5, 2, 0, 13, 2, 22)
        checkLine(perMaisonAndCountry, cpt2++, "PAN - IT", 7, 3, 0, 1, 4, 15)
        checkLine(perMaisonAndCountry, cpt2++, "PAN - NL", 1, 0, 0, 2, 4, 7)
        checkLine(perMaisonAndCountry, cpt2++, "PIA - IT", 3, 7, 0, 0, 4, 14)
        checkLine(perMaisonAndCountry, cpt2++, "PIA - SE", 3, 2, 0, 0, 1, 6)
        checkLine(perMaisonAndCountry, cpt2++, "RDU - IT", 6, 2, 0, 0, 2, 10)
        checkLine(perMaisonAndCountry, cpt2++, "RDU - LU", 2, 2, 0, 0, 3, 7)
        checkLine(perMaisonAndCountry, cpt2++, "VAC - IT", 6, 3, 0, 0, 5, 14)
        checkLine(perMaisonAndCountry, cpt2++, "VCA - IT", 6, 4, 0, 0, 13, 23)
        checkLine(perMaisonAndCountry, cpt2, "VCA - NL", 2, 2, 0, 0, 2, 6)

        then: 'we check the fourth tab, per maison'
        def perMaison = workbook.getSheetAt(5)
        def cpt3 = 0
        checkLine(perMaison, cpt3++, "Maison", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total")
        checkLine(perMaison, cpt3++, "ALS", 10, 4, 0, 0, 2, 16)
        checkLine(perMaison, cpt3++, "BEM", 12, 4, 1, 0, 1, 18)
        checkLine(perMaison, cpt3++, "CAR", 19, 12, 4, 0, 12, 47)
        checkLine(perMaison, cpt3++, "IWC", 13, 5, 1, 1, 4, 24)
        checkLine(perMaison, cpt3++, "JLC", 7, 3, 3, 14, 3, 30)
        checkLine(perMaison, cpt3++, "PAN", 8, 3, 0, 3, 8, 22)
        checkLine(perMaison, cpt3++, "PIA", 6, 9, 0, 0, 5, 20)
        checkLine(perMaison, cpt3++, "RDU", 8, 4, 0, 0, 5, 17)
        checkLine(perMaison, cpt3++, "VAC", 6, 3, 0, 0, 5, 14)
        checkLine(perMaison, cpt3, "VCA", 8, 6, 0, 0, 15, 29)

        then: 'we check the fourth tab, per maison for blockers and critical'
        def perMaisonBlockerCritical = workbook.getSheetAt(6)
        def cpt4 = 0
        checkLine(perMaisonBlockerCritical, cpt4++, "Maison (Block-Crit)", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total")
        checkLine(perMaisonBlockerCritical, cpt4++, "ALS", 8, 3, 0, 0, 2, 13)
        checkLine(perMaisonBlockerCritical, cpt4++, "BEM", 8, 4, 1, 0, 0, 13)
        checkLine(perMaisonBlockerCritical, cpt4++, "CAR", 13, 1, 2, 0, 11, 27)
        checkLine(perMaisonBlockerCritical, cpt4++, "IWC", 9, 4, 1, 1, 3, 18)
        checkLine(perMaisonBlockerCritical, cpt4++, "JLC", 7, 3, 2, 7, 3, 22)
        checkLine(perMaisonBlockerCritical, cpt4++, "PAN", 6, 2, 0, 1, 7, 16)
        checkLine(perMaisonBlockerCritical, cpt4++, "PIA", 4, 8, 0, 0, 4, 16)
        checkLine(perMaisonBlockerCritical, cpt4++, "RDU", 6, 3, 0, 0, 4, 13)
        checkLine(perMaisonBlockerCritical, cpt4++, "VAC", 5, 2, 0, 0, 5, 12)
        checkLine(perMaisonBlockerCritical, cpt4, "VCA", 5, 4, 0, 0, 12, 21)
    }

    static void checkLine(XSSFSheet xssfSheet, int rowIndex, Object... values) {
        def row = xssfSheet.getRow(rowIndex)
        values.eachWithIndex { Object expected, int index ->
            def cell = row.getCell(index)
            if (cell.getCellType() == CellType.NUMERIC) {
                assert cell.getNumericCellValue() == (double) expected
            } else {
                assert cell.getStringCellValue() == expected
            }
        }
    }
}
