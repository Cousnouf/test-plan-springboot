package ch.digity.test_plan.controller

import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification

import java.nio.charset.Charset
import java.time.Clock
import java.time.Instant
import java.time.ZoneId

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
class HelloControllerSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    @SpringBean
    Clock clock = Mock()

    void setup() {
        clock.instant() >> Instant.parse("2021-12-14T15:45:23.89Z")
        clock.getZone() >> ZoneId.of("Europe/Zurich")
    }

    def "should call properly the controller and get the result"() {
        when:
        def resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/hello"))

        then:
        resultActions.andExpect(status().isOk())
        "Application name: Test-plan (Unit test)\n" +
                "Up and running: 2021-12-14T16:45:23.890\n" +
                "Environment: [test]" == resultActions.andReturn().getResponse().getContentAsString(Charset.defaultCharset())
    }
}
