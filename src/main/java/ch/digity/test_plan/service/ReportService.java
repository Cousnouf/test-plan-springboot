package ch.digity.test_plan.service;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.entity.download.ReportRequest;
import ch.digity.test_plan.entity.download.ReportRequestRepository;
import ch.digity.test_plan.writer.TestExcelWriter;
import ch.digity.test_plan.writer.TestExcelWriterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

@Service
public class ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

    private final TestExcelWriterFactory testExcelWriterFactory;
    private final ReportRequestRepository reportRequestRepository;

    public ReportService(TestExcelWriterFactory testExcelWriterFactory, ReportRequestRepository reportRequestRepository) {
        this.testExcelWriterFactory = testExcelWriterFactory;
        this.reportRequestRepository = reportRequestRepository;
    }

    public ReportRequest produceExcelDocument(InputStream inputStream, String filename, LocalDate dateFrom, LocalDate dateTo, NumberFormat numberFormat) {
        try {
            LOGGER.info("Produce excel sheet from date {} to {}", dateFrom, dateTo);
            TestExcelWriter testExcelWriter = testExcelWriterFactory.from(inputStream, dateFrom, dateTo, numberFormat);
            testExcelWriter.writeWorkbook();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            testExcelWriter.writeToOutputStream(bos);
            String fileSimpleName = testExcelWriterFactory.getFileSimpleName(filename);
            byte[] content = bos.toByteArray();
            LOGGER.info("Excel sheet produced, saving into DB");
            ReportRequest reportRequest = new ReportRequest(content, fileSimpleName);
            reportRequestRepository.save(reportRequest);
            LOGGER.info("Excel sheet saved into DB, returning result.");
            return reportRequest;
        } catch (IOException e) {
            LOGGER.error("Error while writing the excel file.", e);
            throw new ExcelDocumentException(e);
        }
    }

    public ReportRequest getExcelDocument(long id) {
        return reportRequestRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Excel document with id %d not found.", id)));
    }

    public List<ReportRequest> getAll() {
        return reportRequestRepository.findAll();
    }
}
