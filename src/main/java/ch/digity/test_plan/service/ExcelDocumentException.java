package ch.digity.test_plan.service;

import java.io.IOException;

public class ExcelDocumentException extends RuntimeException {
    public ExcelDocumentException(IOException e) {
        super(e);
    }
}
