package ch.digity.test_plan.service;

public class ExcelDocumentResult {
    private final byte[] bytes;
    private final String fileSimpleName;

    public ExcelDocumentResult(byte[] bytes, String fileSimpleName) {
        this.bytes = bytes;
        this.fileSimpleName = fileSimpleName;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public String getFileSimpleName() {
        return fileSimpleName;
    }
}
