package ch.digity.test_plan.config;

import ch.digity.test_plan.writer.TestExcelWriterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.time.ZoneId;

@Configuration
public class TestPlanConfiguration {

    @Bean
    Clock clock() {
        return Clock.system(ZoneId.of("Europe/Paris"));
    }

    @Bean
    TestExcelWriterFactory testExcelWriterFactory(Clock clock) {
        return new TestExcelWriterFactory(clock);
    }
}
