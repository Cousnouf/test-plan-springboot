package ch.digity.test_plan.entity.hello;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HelloAccessRepository extends CrudRepository<HelloAccess, Long> {
}