package ch.digity.test_plan.entity.download;

import ch.digity.test_plan.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Lob;

@NoArgsConstructor
@Getter
@Entity
public class ReportRequest extends BaseEntity {

    @Lob
    private byte[] resultContent;

    private String fileSimpleName;

    public ReportRequest(byte[] content, String fileSimpleName) {
        super();
        this.resultContent = content;
        this.fileSimpleName = fileSimpleName;
    }

    public void setResultContent(byte[] resultContent) {
        this.resultContent = resultContent;
    }
}
