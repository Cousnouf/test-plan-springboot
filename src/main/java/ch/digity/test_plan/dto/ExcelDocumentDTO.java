package ch.digity.test_plan.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ExcelDocumentDTO {

    private final long id;
    private final String simpleFileName;
}
