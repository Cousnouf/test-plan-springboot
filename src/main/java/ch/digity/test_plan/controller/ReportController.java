package ch.digity.test_plan.controller;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.dto.ExcelDocumentDTO;
import ch.digity.test_plan.entity.download.ReportRequest;
import ch.digity.test_plan.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Long.compare;
import static java.util.Comparator.comparingLong;


@RestController
@RequestMapping("/report")
public class ReportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

    private final ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping(value = "/generate", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> generateExcelFile(@RequestParam MultipartFile file,
                                                    @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom,
                                                    @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo,
                                                    @RequestParam NumberFormat numberFormat) throws IOException, URISyntaxException {
        ReportRequest reportRequest = reportService.produceExcelDocument(file.getInputStream(), file.getName(), dateFrom, dateTo, numberFormat);
        byte[] resultBytes = reportRequest.getResultContent();
        LOGGER.info("File created, name '{}', size {} bytes", reportRequest.getFileSimpleName(), resultBytes.length);
        return ResponseEntity
                .created(new URI("/report/" + reportRequest.getId()))
                .body(reportRequest.getFileSimpleName());
    }

    @GetMapping
    public ResponseEntity<List<ExcelDocumentDTO>> getAll() {
        return ResponseEntity
                .ok(reportService.getAll().stream()
                        .map(this::toDTO)
                        .sorted(comparingLong(ExcelDocumentDTO::getId))
                        .collect(Collectors.toUnmodifiableList()));
    }

    private ExcelDocumentDTO toDTO(ReportRequest reportRequest) {
        return new ExcelDocumentDTO(reportRequest.getId(), reportRequest.getFileSimpleName());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> getExcelFile(@PathVariable("id") long id) {
        ReportRequest reportRequest = reportService.getExcelDocument(id);
        byte[] resultContent = reportRequest.getResultContent();
        return ResponseEntity.ok()
                .contentLength(resultContent.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment;filename=\"" + reportRequest.getFileSimpleName() + "\"")
                .body(new ByteArrayResource(resultContent));
    }
}
