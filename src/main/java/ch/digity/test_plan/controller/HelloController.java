package ch.digity.test_plan.controller;

import ch.digity.test_plan.entity.hello.HelloAccess;
import ch.digity.test_plan.entity.hello.HelloAccessRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Arrays;

@RestController
@RequestMapping("/hello")
public class HelloController {

    private final Clock clock;
    private final HelloAccessRepository helloAccessRepository;
    private final Environment environment;
    private final String applicatonName;

    public HelloController(Clock clock, HelloAccessRepository helloAccessRepository, Environment environment,
                           @Value("${test-plan.application.name}") String applicatonName) {
        this.clock = clock;
        this.helloAccessRepository = helloAccessRepository;
        this.environment = environment;
        this.applicatonName = applicatonName;
    }

    @GetMapping
    public String index() {
        helloAccessRepository.save(new HelloAccess());
        return "Application name: " + applicatonName + "\n" +
                "Up and running: " + LocalDateTime.now(clock) + "\n" +
                "Environment: " + Arrays.toString(environment.getActiveProfiles());
    }
}
