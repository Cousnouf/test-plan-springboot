const Upload = function (file) {
    this.file = file;
};

Upload.prototype.getType = function () {
    return this.file.type;
};
Upload.prototype.getSize = function () {
    return this.file.size;
};
Upload.prototype.getName = function () {
    return this.file.name;
};

function addResultToTable(linkText, hrefValue) {
    let results = $('#results');
    const resultTableContent = results.html();
    results.html(
        '<tr>' +
        '   <td><a href="' + hrefValue + '">' + linkText + '</a></td>' +
        '</tr>\n' +
        resultTableContent
    );
}

Upload.prototype.doUpload = function () {
    const that = this;
    const formData = new FormData();

    // add assoc key values, this will be posts values
    formData.append("file", this.file, this.getName());
    formData.append("upload_file", true);

    let urlWithParams = '/report/generate?dateFrom=' + $('#dateFrom').val() + '&dateTo=' + $('#dateTo').val() + '&numberFormat=' + $('#numberFormat').val();
    console.log(urlWithParams)

    resetMessages();
    $.ajax({
        type: "POST",
        url: urlWithParams,
        xhr: function () {
            const myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data, textStatus, request) {
            $('#generating').hide();
            $('#generatingSuccess').show();
            addResultToTable(data, request.getResponseHeader('Location'));
        },
        error: function (error) {
            alert(error.responseJSON.error);
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    let percent = 0;
    const position = event.loaded || event.position;
    const total = event.total;
    const progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
    if (percent === 100) {
        $('#generating').show();
    }
};

function resetMessages() {
    $('#generating').hide();
    $('#generatingSuccess').hide();
}

function populateDocumentList() {
    $.ajax({
        type: "GET",
        url: "/report",
        success: function (excelDocuments) {
            excelDocuments.forEach(function (excelDocument) {
                addResultToTable(excelDocument.simpleFileName, '/report/' + excelDocument.id)
            })
        }
    })
}

const INPUT_FILE_LABEL = 'Input file:';

function validateFileInput(file) {
    let inputFileLabel = $('#inputFileLabel');
    if (file === undefined) {
        inputFileLabel.html(INPUT_FILE_LABEL + ' <font color="red">Please choose a file!</font>');
        return false;
    }
    inputFileLabel.html(INPUT_FILE_LABEL);
    return true;
}

function validateDate(dateInput) {
    const val = dateInput.val()
    if (val.trim().length > 0 && !(/^(\d{4}-\d{2}-\d{2})$/.test(val))) {
        dateInput.css({'background-color': '#e0709c'});
        return false;
    }
    dateInput.css({'background-color': '#ffffff'});
    return true;
}

$(document).ready(function () {
    resetMessages();
    populateDocumentList();
    $('#sendButton').click(function () {
        var file = $("#csvFile")[0].files[0];
        const validInputs = [validateFileInput(file), validateDate($('#dateFrom')), validateDate($('#dateTo'))];
        if (validInputs.some(valid => !valid)) {
            return;
        }

        const upload = new Upload(file);
        upload.doUpload();
    });
});